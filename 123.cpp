#include <iostream>
#include <fstream>

int main() {
    std::ofstream outFile("example.bin", std::ios::binary);
    if (!outFile) {
        std::cerr << "Невозможно открыть файл для записи." << std::endl;
        return 1;
    }

    int data = 123; // Пример данных для записи
    outFile.write(reinterpret_cast<char*>(&data), sizeof(data));
    if (!outFile.good()) {
        std::cerr << "Ошибка записи в файл." << std::endl;
        return 1;
    }

    outFile.close();
    std::cout << "Файл успешно создан." << std::endl;
    return 0;
}
